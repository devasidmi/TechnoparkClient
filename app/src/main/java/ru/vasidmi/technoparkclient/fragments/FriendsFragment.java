package ru.vasidmi.technoparkclient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.MainActivity;
import ru.vasidmi.technoparkclient.NavigationDrawer;
import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.adapters.FriendsListAdapter;
import ru.vasidmi.technoparkclient.models.friends.FriendsModel;

/**
 * Created by vasidmi on 23/03/2018.
 */

public class FriendsFragment extends Fragment {

    public static final String TAG = "FriendsFragment";
    private View view;
    private FriendsModel friendsModel;
    private NavigationDrawer mNavigationDrawer;

    public FriendsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.friends_fragment, container, false);
        MainActivity controller = (MainActivity) getActivity();
        BusProvider.getBus().post(new Events.ChangeToolbarTitle(R.string.navmenu_friends));
        if (controller != null) {
            mNavigationDrawer = (controller).getNavigationDrawer();
        }
        if (mNavigationDrawer == null) {
            BusProvider.getBus().post(new Events.CreateNavigationDrawer(4));
        } else {

            if (mNavigationDrawer.isDestroyed) {
                BusProvider.getBus().post(new Events.DisableBackArrow());
                mNavigationDrawer.restore();
                mNavigationDrawer.updateNavigationDrawerHeader();
                mNavigationDrawer.setSelectedNavigationDrawerItem(4);
            }
        }
        if (controller != null)
            friendsModel = controller.getFriends();
        initFriendsList();
        return view;
    }

    private void initFriendsList() {
        FriendsListAdapter mFriendsListAdapter = new FriendsListAdapter(getContext(), friendsModel);
        RecyclerView mFriendsRecyclerView = view.findViewById(R.id.friends_list);
        mFriendsRecyclerView.setAdapter(mFriendsListAdapter);
        mFriendsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
