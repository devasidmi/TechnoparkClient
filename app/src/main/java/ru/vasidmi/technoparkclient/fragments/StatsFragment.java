package ru.vasidmi.technoparkclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import retrofit2.Call;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Callback;
import retrofit2.Response;
import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.MainActivity;
import ru.vasidmi.technoparkclient.NavigationDrawer;
import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.adapters.TapeListAdapter;
import ru.vasidmi.technoparkclient.api.ApiService;
import ru.vasidmi.technoparkclient.models.stats.Achievement;
import ru.vasidmi.technoparkclient.models.stats.PortalActivity;
import ru.vasidmi.technoparkclient.models.stats.StatsModel;

public class StatsFragment extends Fragment {

    public static final String TAG = "StatsFragment";

    AppCompatImageView powerPhoto;
    TextView powerName, powerValue;
    private PortalActivity portalActivity;
    private Context context;
    private StatsModel stats;
    private List<Achievement> achievement;
    private View v;
    Map<String, List<Achievement>> mapAchievement = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.stats_fragment, container, false);
        context = getContext();
        MainActivity controller = (MainActivity) getActivity();
        statsResponse();//показать фотки
        if (controller != null) {
            NavigationDrawer navigationDrawer = (controller).getNavigationDrawer();
            BusProvider.getBus().post(new Events.ChangeToolbarTitle(navigationDrawer.getUserName()));
        }
            powerName = v.findViewById(R.id.power);
            powerName.setText(R.string.power_name);
            powerPhoto = v.findViewById(R.id.power_photo);
            Picasso.get()
                    .load(R.drawable.ic_fitness_center_black_36dp)
                    .fit()
                    .centerCrop()
                    .into(powerPhoto);
            powerValue = v.findViewById(R.id.power_value);
            AppCompatTextView attendance = v.findViewById(R.id.attendance);
            attendance.setText(R.string.attendance_name);
            AppCompatTextView curriculum = v.findViewById(R.id.curriculum);
            curriculum.setText(R.string.curriculum_name);
            AppCompatTextView portal= v.findViewById(R.id.portal);
            portal.setText(R.string.portal_name);
            AppCompatTextView response = v.findViewById(R.id.response);
            response.setText(R.string.response_name);
            AppCompatTextView trainee = v.findViewById(R.id.trainee);
            trainee.setText(R.string.trainee_name);
        return v;
    }

    private void setValues() {
        powerValue.setText(portalActivity.getPower());

        List<Achievement> localAttendance = mapAchievement.get("attendance");
        List<Achievement> localCurriculum = mapAchievement.get("curriculum");
        List<Achievement> localPortal = mapAchievement.get("portal");
        List<Achievement> localResponse = mapAchievement.get("response");
        List<Achievement> localTrainee = mapAchievement.get("trainee");

        setAdapter(localAttendance, R.id.tape_attendance);
        setAdapter(localCurriculum, R.id.tape_curriculum);
        setAdapter(localPortal, R.id.tape_portal);
        setAdapter(localResponse, R.id.tape_response);
        setAdapter(localTrainee, R.id.tape_trainee);
    }

    private void setAdapter(List<Achievement> localVariable, int id) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        TapeListAdapter tapeListAdapter = new TapeListAdapter(context, localVariable, localVariable.size());
        RecyclerView recyclerView = v.findViewById(id);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(tapeListAdapter);
    }

    private void countItems(List<Achievement> string) {
        ArrayList<Achievement> attendance = new ArrayList<>();
        ArrayList<Achievement> curriculum = new ArrayList<>();
        ArrayList<Achievement> portal= new ArrayList<>();
        ArrayList<Achievement> response = new ArrayList<>();
        ArrayList<Achievement> trainee = new ArrayList<>();
        if (string != null) {
            String item;
            for (int i = 0; i < string.size(); ++i) {
                item = string.get(i).getCategory();
                if (isAdded()) {
                    if (item.equals(getResources().getString(R.string.attendance_items))) {
                        attendance.add(string.get(i));
                    }

                    if (item.equals(getResources().getString(R.string.curriculum_items))) {
                        curriculum.add(string.get(i));
                    }
                    if (item.equals(getResources().getString(R.string.portal_items))) {
                        portal.add(string.get(i));
                    }
                    if (item.equals(getResources().getString(R.string.response_items))) {
                        response.add(string.get(i));
                    }
                    if (item.equals(getResources().getString(R.string.trainee_items))) {
                        trainee.add(string.get(i));
                    }
                }
            }
            mapAchievement.put("attendance", attendance);
            mapAchievement.put("curriculum",curriculum);
            mapAchievement.put("portal", portal);
            mapAchievement.put("response", response);
            mapAchievement.put("trainee", trainee);
        }
    }

    private void statsResponse() {
        ApiService service = ApiService.retrofit.create(ApiService.class);
        if (context != null) {
            String authHeader = context.getSharedPreferences("session_prefs", Context.MODE_PRIVATE)
                    .getString("auth_header", null);
            Call<StatsModel> call = service.getStats(authHeader);
            call.enqueue(new Callback<StatsModel>() {
                @Override
                public void onResponse(Call<StatsModel> call, Response<StatsModel> response) {
                    StatsModel statsModel = response.body();
                    if (statsModel != null) {
                        stats = statsModel;
                        achievement = stats.getAchievements();
                        portalActivity = stats.getPortalActivity();
                        countItems(achievement);
                        setValues();
                    }
                }

                @Override
                public void onFailure(Call<StatsModel> call, Throwable t) {
                    Log.e("tag","err");
                }
            });
        }
    }
}
