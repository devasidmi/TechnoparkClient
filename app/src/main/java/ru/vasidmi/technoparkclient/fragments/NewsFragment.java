package ru.vasidmi.technoparkclient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.MainActivity;
import ru.vasidmi.technoparkclient.NavigationDrawer;
import ru.vasidmi.technoparkclient.R;
import java.util.List;

import ru.vasidmi.technoparkclient.adapters.NewsListAdapter;
import ru.vasidmi.technoparkclient.models.NewsModel;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class NewsFragment extends Fragment {

    public static final String TAG = "NewsFragment";
    private NavigationDrawer mNavigationDrawer;
    public List<NewsModel> newsModel;
    public NewsListAdapter newsListAdapter;
    public RecyclerView recyclerView;

    public NewsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_fragment, container, false);
        BusProvider.getBus().post(new Events.ChangeToolbarTitle(R.string.navmenu_news));
        MainActivity controller = (MainActivity) getActivity();
        if (getActivity() != null) {
            mNavigationDrawer = ((MainActivity) getActivity()).getNavigationDrawer();
        }
        if (mNavigationDrawer == null) {
            BusProvider.getBus().post(new Events.CreateNavigationDrawer(1));
        } else {
            if (mNavigationDrawer.isDestroyed) {
                mNavigationDrawer.restore();
                mNavigationDrawer.updateNavigationDrawerHeader();
                mNavigationDrawer.setSelectedNavigationDrawerItem(1);
            }
        }
        if (controller != null)
            newsModel = controller.getNews();
        newsListAdapter = new NewsListAdapter(getContext(), newsModel);
        recyclerView = view.findViewById(R.id.news_list);
        recyclerView.setAdapter(newsListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }
}
