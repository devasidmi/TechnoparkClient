package ru.vasidmi.technoparkclient.fragments;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.models.NewsModel;

public class CurrentNews extends Fragment implements View.OnClickListener {

    public static final String TAG = "CurrentNews";
    private AppCompatTextView title, category, content, created, author;
    private CircularImageView photo;
    private NewsModel newsModel;
    private long clickNotAllowed;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.current_news, container, false);
        clickNotAllowed = 0;
        if (getArguments() != null) {
            if (getArguments().size() > 0) {
                newsModel = (NewsModel) getArguments().get("current_news");
                onSaveInstanceState(getArguments());
            }
        }

            if (newsModel != null) {
                title = view.findViewById(R.id.title);
                category = view.findViewById(R.id.category);
                content = view.findViewById(R.id.content);
                created = view.findViewById(R.id.created);
                author = view.findViewById(R.id.author);
                photo = view.findViewById(R.id.photo);

                photo.setOnClickListener(this);
                author.setOnClickListener(this);

                BusProvider.getBus().post(new Events.ChangeToolbarTitle("Подробнее"));
                BusProvider.getBus().post(new Events.DestroyNavigationDrawer());
                BusProvider.getBus().post(new Events.EnableBackArrow());

                setupUI(newsModel);

                clearGetArguments();
            }
            else {
                clearGetArguments();
            }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null)
            newsModel = (NewsModel) savedInstanceState.getSerializable("current_news");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable("current_news", newsModel);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void clearGetArguments() {
        if (getArguments() != null)
            getArguments().clear();
    }

    private void setupUI(NewsModel newsModel) {
        title.setText(newsModel.getTitle());
        category.setText(newsModel.getCategory());
        content.setText(newsModel.getContent());
        author.setText(newsModel.getAuthor());
        created.setText(newsModel.getCreated());

        Picasso.get()
                .load(newsModel.getAuthorPhoto())
                .error(R.drawable.user_not_found)
                .fit()
                .centerCrop()
                //.centerInside()
                //.resize(50,50)
                .into(photo);

    }

    public Fragment newInstance(NewsModel newsModel) {
        Bundle args = new Bundle();
        args.putSerializable("current_news", newsModel);

        CurrentNews currentNews = new CurrentNews();
        currentNews.setArguments(args);
        return currentNews;
    }

    @Override
    public void onClick(View v) {
        ProfileFragment profileFragment = new ProfileFragment();
        if (SystemClock.elapsedRealtime() - clickNotAllowed < 1000) {
            return;
        }
        switch (v.getId()) {
            case R.id.photo:
                BusProvider.getBus().post(new Events.OpenScreen(profileFragment.newInstance(newsModel), ProfileFragment.TAG, true));
                clickNotAllowed = SystemClock.elapsedRealtime();
                break;
            case R.id.author:
                BusProvider.getBus().post(new Events.OpenScreen(profileFragment.newInstance(newsModel), ProfileFragment.TAG, true));
                clickNotAllowed = SystemClock.elapsedRealtime();
                break;
        }
    }
}
