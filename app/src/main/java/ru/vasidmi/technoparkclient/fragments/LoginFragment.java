package ru.vasidmi.technoparkclient.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import ru.vasidmi.technoparkclient.Auth;
import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.R;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class LoginFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "LoginFragment";

    private AppCompatButton loginBtn;
    private ProgressBar loginProgressBar;
    private AppCompatEditText emailInput, passwordInput;
    private View v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.login_fragment, container, false);
        BusProvider.getBus().post(new Events.ChangeToolbarTitle(R.string.appbar_login));
        if (getArguments() != null) {
            if (getArguments().getBoolean("destroy")) {
                BusProvider.getBus().post(new Events.DestroyNavigationDrawer());

            } else {
                BusProvider.getBus().post(new Events.SetupToolbar());
            }
        }
        emailInput = v.findViewById(R.id.emailInput);
        passwordInput = v.findViewById(R.id.passwordInput);
        loginBtn = v.findViewById(R.id.loginBtn);
        AppCompatButton kulikBtn = v.findViewById(R.id.kulik);
        AppCompatButton pashokBtn = v.findViewById(R.id.pashok);
        loginProgressBar = v.findViewById(R.id.loginProgressBar);
        loginBtn.setOnClickListener(this);
        kulikBtn.setOnClickListener(this);
        pashokBtn.setOnClickListener(this);
        return v;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        loginBtn = null;
        v = null;
        loginProgressBar = null;
    }

    public LoginFragment newInstance(boolean destroy) {

        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        args.putBoolean("destroy", destroy);
        fragment.setArguments(args);

        return fragment;
    }

    public void showLoginError(String message) {
        Snackbar.make(v, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:
                disableLoginBtn();
                String email, password;
                email = emailInput.getText().toString().trim();
                password = passwordInput.getText().toString().trim();
                String authHeader = new Auth().getAuthHeader(email + ":" + password);
                BusProvider.getBus().post(new Events.LoadMainData(authHeader));
                break;
            case R.id.kulik:
                emailInput.setText(getResources().getString(R.string.kulikof_name));
                passwordInput.setText(getResources().getString(R.string.kulikof_password));
                break;
            case R.id.pashok:
                emailInput.setText(getResources().getString(R.string.pashok_name));
                passwordInput.setText(getResources().getString(R.string.pashok_password));
        }
    }

    public void enableLoginBtn() {
        loginBtn.setEnabled(true);
        Log.e("tag2", "hi");
        loginProgressBar.setVisibility(View.GONE);
    }

    private void disableLoginBtn() {
        loginBtn.setEnabled(false);
        Log.e("tag1", loginBtn.getText().toString());
        loginProgressBar.setVisibility(View.VISIBLE);
    }
}
