package ru.vasidmi.technoparkclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.api.ApiService;
import ru.vasidmi.technoparkclient.models.NewsModel;
import ru.vasidmi.technoparkclient.models.ProfileModel;
import ru.vasidmi.technoparkclient.models.friends.Friend;

public class ProfileFragment extends Fragment {

    public static final String TAG = "ProfileFragment";
    private AppCompatTextView userAbout;
    private ProgressBar friendProfileProgressBar;
    Friend friendProfile;
    NewsModel newsProfile;
    private ArrayList<String> arrayList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.friend_profile_fragment, container, false);
        CircularImageView userAvatar = view.findViewById(R.id.user_avatar);
        userAbout = view.findViewById(R.id.about_text);
        friendProfileProgressBar = view.findViewById(R.id.friendProfileProgressBar);
        arrayList = new ArrayList<>();
        if (analyzeVars() != null) {
            getProfile(arrayList.get(0));
            BusProvider.getBus().post(new Events.ChangeToolbarTitle(arrayList.get(1)));
            BusProvider.getBus().post(new Events.DestroyNavigationDrawer());
            BusProvider.getBus().post(new Events.EnableBackArrow());

            Picasso.get()
                    .load(arrayList.get(2))
                    .error(R.drawable.user_not_found)
                    .fit()
                    .centerCrop()
                    //.resize(160, 160)
                    //.centerInside()
                    .into(userAvatar);

            clearGetArguments();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.get("friend") != null) {
                friendProfile = (Friend) savedInstanceState.getSerializable("friend");
            }
            if (savedInstanceState.get("news") != null) {
                newsProfile = (NewsModel) savedInstanceState.getSerializable("news");
            }
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (getArguments() != null) {
            if (getArguments().get("news") != null) {
                outState.putSerializable("news", newsProfile);
            }
            if (getArguments().get("friend") != null) {
                outState.putSerializable("friend", friendProfile);
            }
        }
        super.onSaveInstanceState(outState);
    }

    private ArrayList<String> analyzeVars() {
        if (getArguments() != null && getArguments().size() > 0) {
                if (getArguments().get("news") != null) {
                    newsProfile = (NewsModel) getArguments().get("news");
                    if (newsProfile != null) {
                        arrayList.add(newsProfile.getAuthorId());
                        arrayList.add(newsProfile.getAuthor());
                        arrayList.add(newsProfile.getAuthorPhoto());
                    }
                } if (getArguments().get("friend") != null) {
                    friendProfile = (Friend) getArguments().get("friend");
                    if (friendProfile != null) {
                        arrayList.add(friendProfile.getUserId());
                        arrayList.add(friendProfile.getFullname());
                        arrayList.add(friendProfile.getAvatarUrl());
                    }
                }
            onSaveInstanceState(getArguments());
        } else
            arrayList = null;
        return arrayList;
    }

    private void clearGetArguments() {
        if (getArguments() != null)
            getArguments().clear();
    }

    private void setupUI(ProfileModel friendProfile) {
        friendProfileProgressBar.setVisibility(View.GONE);
        userAbout.setText(friendProfile.getUserAbout());
    }

    private void getProfile(String userId) {
        ApiService service = ApiService.retrofit.create(ApiService.class);
        Context context = getContext();
        if (context != null) {
            String authHeader = getContext().getSharedPreferences("session_prefs", Context.MODE_PRIVATE)
                    .getString("auth_header", null);
            if (authHeader != null) {
                Call<ProfileModel> call = service.getUserProfile(authHeader, userId);
                call.enqueue(new Callback<ProfileModel>() {
                    @Override
                    public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                        ProfileModel profileModel = response.body();
                        if (profileModel != null) {
                            setupUI(profileModel);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfileModel> call, Throwable t) {
                        Log.e("YAY!", "Error in getting friend profile");
                    }
                });
            }
        }
    }

    public ProfileFragment newInstance(Object profile) {

        Bundle args = new Bundle();
        if (profile instanceof Friend)
            args.putSerializable("friend", (Serializable) profile);
        else
            args.putSerializable("news", (Serializable) profile);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
