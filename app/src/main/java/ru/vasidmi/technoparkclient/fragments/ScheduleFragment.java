package ru.vasidmi.technoparkclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.MainActivity;
import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.adapters.ScheduleListAdapter;
import ru.vasidmi.technoparkclient.api.ApiService;
import ru.vasidmi.technoparkclient.models.ScheduleModel;

/**
 * Created by vasidmi on 26/03/2018.
 */

public class ScheduleFragment extends Fragment {

    public static final String TAG = "ScheduleFragment";
    private View view;
    private ProgressBar scheduleProgressBar;
    private MainActivity controller;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.schedule_fragment, container, false);
        controller = (MainActivity) getActivity();
        scheduleProgressBar = view.findViewById(R.id.scheduleProgressBar);
        BusProvider.getBus().post(new Events.ChangeToolbarTitle(R.string.navmenu_schedule));
        loadSchedule();
        return view;
    }

    private void setupUI(View view, List<ScheduleModel> schedule) {
        RecyclerView scheduleList = view.findViewById(R.id.schedule_list);
        ScheduleListAdapter scheduleListAdapter = new ScheduleListAdapter(getContext(), schedule);
        scheduleList.setLayoutManager(new LinearLayoutManager(getContext()));
        scheduleList.setAdapter(scheduleListAdapter);
    }

    private void loadSchedule() {
        if (controller.getSchedule() == null) {
            ApiService service = ApiService.retrofit.create(ApiService.class);
            Context context = getContext();
            if (context != null) {
                String authHeader = getContext().getSharedPreferences("session_prefs", Context.MODE_PRIVATE)
                        .getString("auth_header", null);
                if (authHeader != null) {
                    scheduleProgressBar.setVisibility(View.VISIBLE);
                    Call<List<ScheduleModel>> call = service.getSchedule(authHeader);
                    call.enqueue(new Callback<List<ScheduleModel>>() {
                        @Override
                        public void onResponse(Call<List<ScheduleModel>> call, Response<List<ScheduleModel>> response) {
                            scheduleProgressBar.setVisibility(View.GONE);
                            controller.setSchedule(response.body());
                            setupUI(view, response.body());
                        }

                        @Override
                        public void onFailure(Call<List<ScheduleModel>> call, Throwable t) {
                            scheduleProgressBar.setVisibility(View.GONE);
                            Log.e("YAY!", "Can't get schedule!");
                        }
                    });
                }
            }
        } else {
            List<ScheduleModel> schedule = controller.getSchedule();
            setupUI(view, schedule);
        }
    }
}
