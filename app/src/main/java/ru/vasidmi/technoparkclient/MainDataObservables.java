package ru.vasidmi.technoparkclient;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.vasidmi.technoparkclient.api.ApiService;
import ru.vasidmi.technoparkclient.models.NewsModel;
import ru.vasidmi.technoparkclient.models.ProfileModel;
import ru.vasidmi.technoparkclient.models.friends.FriendsModel;
import ru.vasidmi.technoparkclient.models.progress.ProgressModel;

/**
 * Created by vasidmi on 19/03/2018.
 */

class MainDataObservables {

    Observable<List<NewsModel>> newsObser;
    Observable<ProfileModel> profileObser;
    Observable<FriendsModel> friendsObser;
    Observable<List<ProgressModel>> progressObser;

    MainDataObservables(String authHeader) {
        loadObservables(authHeader);
    }


    private void loadObservables(String authHeader) {
        initObservables(authHeader);
    }

    private void initObservables(String authHeader) {
        newsObser = ApiService.retrofit
                .create(ApiService.class)
                .getNews(authHeader)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        profileObser = ApiService.retrofit
                .create(ApiService.class)
                .getOwnProfile(authHeader)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        friendsObser = ApiService.retrofit
                .create(ApiService.class)
                .getOwnFriends(authHeader)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        progressObser = ApiService.retrofit
                .create(ApiService.class)
                .getProgress(authHeader)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
