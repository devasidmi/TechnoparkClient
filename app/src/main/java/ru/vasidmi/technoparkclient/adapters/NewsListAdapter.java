package ru.vasidmi.technoparkclient.adapters;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.fragments.CurrentNews;
import ru.vasidmi.technoparkclient.fragments.ProfileFragment;
import ru.vasidmi.technoparkclient.models.NewsModel;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    private Context context;
    private List<NewsModel> newsModel;
    private long clickNotAllowed;

    public NewsListAdapter(Context context, List<NewsModel> newsModel) {
        this.context = context;
        this.newsModel = newsModel;
        clickNotAllowed = 0;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.news_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(newsModel.get(position).getAuthorPhoto())
                .fit()
                //.resize(50,50)
                .error(R.drawable.user_not_found)
                .centerCrop()
                .into(holder.photo);
        holder.title.setText(newsModel.get(position).getTitle());
        holder.content.setText(cutText(newsModel.get(position).getContent()));
        holder.category.setText(newsModel.get(position).getCategory());
        holder.author.setText(newsModel.get(position).getAuthor());
        holder.created.setText(newsModel.get(position).getCreated());
    }

    @Override
    public int getItemCount() {
        return newsModel.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView title, category, content, created, author;
        CircularImageView photo;

        ViewHolder(View itemView) {
            super(itemView);
            RelativeLayout relativeLayout = itemView.findViewById(R.id.relative);
            relativeLayout.setOnClickListener(this);
            title = itemView.findViewById(R.id.title);
            category = itemView.findViewById(R.id.category);
            content = itemView.findViewById(R.id.content);
            created = itemView.findViewById(R.id.created);
            author = itemView.findViewById(R.id.author);
            photo = itemView.findViewById(R.id.photo);
            photo.setOnClickListener(this);
            author.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (SystemClock.elapsedRealtime() - clickNotAllowed < 1000) {
                return;
            }
            ProfileFragment profileFragment = new ProfileFragment();
            CurrentNews currentNews = new CurrentNews();
            NewsModel currentEvent;
            switch (v.getId()) {
                case R.id.relative:
                    currentEvent = newsModel.get(getAdapterPosition());
                    BusProvider.getBus().post(new Events.OpenScreen(currentNews.newInstance(currentEvent), CurrentNews.TAG, true));
                    break;
                case R.id.photo:
                    currentEvent = newsModel.get(getAdapterPosition());
                    BusProvider.getBus().post(new Events.OpenScreen(profileFragment.newInstance(currentEvent), ProfileFragment.TAG, true));
                    clickNotAllowed = SystemClock.elapsedRealtime();
                    break;
                case R.id.author:
                    currentEvent = newsModel.get(getAdapterPosition());
                    BusProvider.getBus().post(new Events.OpenScreen(profileFragment.newInstance(currentEvent), ProfileFragment.TAG, true));
                    clickNotAllowed = SystemClock.elapsedRealtime();
                    break;
            }
        }
    }

    private String cutText(String text) {
        if (text.length() >= 330) {
            text = text.substring(0, 329) + "...";
        }
       return text;
    }
}
