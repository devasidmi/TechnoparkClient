package ru.vasidmi.technoparkclient.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.models.stats.Achievement;

public class TapeListAdapter extends RecyclerView.Adapter<TapeListAdapter.ViewHolder>{

    private Context context;
    private List<Achievement> achievement;
    private Integer count;

    public TapeListAdapter(Context context, List<Achievement> achievement, Integer count) {
        this.context = context;
        this.achievement = achievement;
        this.count = count;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.attendance_item,parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(achievement.get(position).getImg())
                .fit()
                .centerCrop()
                //.resize(600,200)
                //.centerInside()
                .into(holder.photo);
    }

    @Override
    public int getItemCount() {
        return count;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        CircularImageView photo;


        ViewHolder(View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.photo);
        }
    }
}
