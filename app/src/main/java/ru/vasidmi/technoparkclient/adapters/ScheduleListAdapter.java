package ru.vasidmi.technoparkclient.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.models.ScheduleModel;

public class ScheduleListAdapter extends RecyclerView.Adapter<ScheduleListAdapter.ViewHolder> {

    private Context context;
    private List<ScheduleModel> schedule;

    public ScheduleListAdapter(Context context, List<ScheduleModel> schedule) {
        this.context = context;
        this.schedule = schedule;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.subject_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ScheduleModel model = schedule.get(position);
        Picasso.get()
                .load(model.getTutorImage())
                .fit()
                .centerCrop()
                //.resize(80, 80)
                //.centerInside()
                .error(R.drawable.user_not_found)
                .into(holder.tutorAvatar);
        holder.tutor.setText(model.getTutorName());
        holder.lesson_title.setText(model.getDiscipline());
        holder.lection_title.setText(model.getLesson());
        holder.time.setText(model.getStartTime().concat("-").concat(model.getEndTime()));
        holder.place.setText(model.getPlace());
        holder.group.setText(model.getGroups());
    }

    @Override
    public int getItemCount() {
        return schedule.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CircularImageView tutorAvatar;
        AppCompatImageView timeImg;
        AppCompatTextView tutor, lesson_title, lection_title, time, place, group;

        ViewHolder(View itemView) {
            super(itemView);
            tutorAvatar = itemView.findViewById(R.id.tutorAvatar);
            tutor = itemView.findViewById(R.id.tutorFullname);
            lesson_title = itemView.findViewById(R.id.lesson_title);
            lection_title = itemView.findViewById(R.id.lection_title);
            time = itemView.findViewById(R.id.time_label);
            timeImg = itemView.findViewById(R.id.time_img);
            place = itemView.findViewById(R.id.place_label);
            group = itemView.findViewById(R.id.group_label);
        }
    }
}
