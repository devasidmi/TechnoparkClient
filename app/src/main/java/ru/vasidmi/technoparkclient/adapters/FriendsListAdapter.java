package ru.vasidmi.technoparkclient.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.balysv.materialripple.MaterialRippleLayout;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import ru.vasidmi.technoparkclient.BusProvider;
import ru.vasidmi.technoparkclient.Events;
import ru.vasidmi.technoparkclient.R;
import ru.vasidmi.technoparkclient.fragments.ProfileFragment;
import ru.vasidmi.technoparkclient.models.friends.Friend;
import ru.vasidmi.technoparkclient.models.friends.FriendsModel;

/**
 * Created by vasidmi on 23/03/2018.
 */

public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ViewHolder> {

    private Context context;
    private FriendsModel friends;
    private List<Friend> friends_list;

    public FriendsListAdapter(Context context, FriendsModel friends) {
        this.context = context;
        this.friends = friends;
        friends_list = friends.getFriends();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.friend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        Picasso.get()
                .load(friends_list.get(position).getAvatarUrl())
                .fit()
                .centerCrop()
                //.resize(80, 80)
                //.centerInside()
                .error(R.drawable.user_not_found)
                .into(holder.avatar);

        holder.fullname.setText(friends_list.get(position).getFullname());
        holder.power.setText(friends_list.get(position).getPower());
        holder.rating.setText(friends_list.get(position).getRating());
    }

    @Override
    public int getItemCount() {
        return Integer.parseInt(friends.getCount());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AppCompatTextView fullname, power, rating;
        CircularImageView avatar;

        ViewHolder(View itemView) {
            super(itemView);
            MaterialRippleLayout friendItem = itemView.findViewById(R.id.friend_item);
            friendItem.setOnClickListener(this);
//            RelativeLayout relativeLayout = itemView.findViewById(R.id.relative);
//            relativeLayout.setOnClickListener(this);
            fullname = itemView.findViewById(R.id.user_fullname);
            power = itemView.findViewById(R.id.powerValue);
            rating = itemView.findViewById(R.id.ratingValue);
            avatar = itemView.findViewById(R.id.user_avatar);
        }

        @Override
        public void onClick(View view) {
                int friendId = this.getAdapterPosition();
                Friend friendData = friends_list.get(friendId);
            ProfileFragment profileFragment = new ProfileFragment();
                BusProvider.getBus().post(new Events.OpenScreen(profileFragment.newInstance(friendData), ProfileFragment.TAG, true));
        }
    }
}
