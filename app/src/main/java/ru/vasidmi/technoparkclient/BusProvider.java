package ru.vasidmi.technoparkclient;

import com.squareup.otto.Bus;

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getBus() {
        return BUS;
    }
}
