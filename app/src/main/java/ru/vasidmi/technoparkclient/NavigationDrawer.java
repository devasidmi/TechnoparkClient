package ru.vasidmi.technoparkclient;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import ru.vasidmi.technoparkclient.fragments.FriendsFragment;
import ru.vasidmi.technoparkclient.fragments.LoginFragment;
import ru.vasidmi.technoparkclient.fragments.NewsFragment;
import ru.vasidmi.technoparkclient.fragments.ScheduleFragment;
import ru.vasidmi.technoparkclient.fragments.StatsFragment;

/**
 * Created by vasidmi on 22/03/2018.
 */

public class NavigationDrawer implements Drawer.OnDrawerItemClickListener, Drawer.OnDrawerNavigationListener, View.OnClickListener {

    private Drawer mDrawer;
    private AppCompatTextView profileName, groups;
    private CircularImageView userAvatar;
    private DividerDrawerItem menu_divider = new DividerDrawerItem();
    private PrimaryDrawerItem newsItem = new PrimaryDrawerItem().withIdentifier(1).withName(R.string.navmenu_news);
    private PrimaryDrawerItem scheduleItem = new PrimaryDrawerItem().withIdentifier(2).withName(R.string.navmenu_schedule);
    private PrimaryDrawerItem progressItem = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.navmenu_progress);
    private PrimaryDrawerItem friendsItem = new PrimaryDrawerItem().withIdentifier(4).withName(R.string.navmenu_friends);
    private SecondaryDrawerItem settings = new SecondaryDrawerItem().withIdentifier(5).withName(R.string.navmenu_settings);
    private SecondaryDrawerItem about = new SecondaryDrawerItem().withIdentifier(6).withName(R.string.navmenu_about);
    private SecondaryDrawerItem logout = new SecondaryDrawerItem().withIdentifier(7).withName(R.string.navmenu_logout);
    private Context mContext;
    public boolean isDestroyed = false;
    private MainActivity controller;

    NavigationDrawer(Context context) {
        mContext = context;
    }

    public String getUserName() {
        return controller.getProfiles().getFullname();
    }

    public void createDrawer(Activity activity, int rootView, int selectedItemId) {

        controller = ((MainActivity) activity);

        mDrawer = new DrawerBuilder()
                .withActivity(activity)
                .withRootView(rootView)
                .withToolbar(controller.getToolbar())
                .withDisplayBelowStatusBar(false)
                .withGenerateMiniDrawer(true)
                .withHeader(R.layout.navigation_header)
                .withHeaderDivider(true)
                .withSelectedItemByPosition(selectedItemId)
                .withDrawerWidthDp(mContext.getResources().getInteger(R.integer.drawer_width))
                .withOnDrawerNavigationListener(this)
                .addDrawerItems(newsItem, scheduleItem, progressItem, friendsItem, menu_divider, settings, about, logout)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .build();
        View headerView = mDrawer.getHeader();
        profileName = headerView.findViewById(R.id.profileName);
        groups = headerView.findViewById(R.id.mainGroupLabel);
        userAvatar = headerView.findViewById(R.id.user_avatar);
        updateNavigationDrawerHeader();

        mDrawer.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        isDestroyed = false;
        mDrawer.setOnDrawerItemClickListener(this);
    }

    public void updateNavigationDrawerHeader() {
        Picasso.get()
                .load(controller.getProfiles().getAvatarUrl())
                .fit()
                .centerCrop()
                //.resize(6000, 2000)
                //.centerInside()
                .error(R.drawable.user_not_found)
                .into(userAvatar);

        profileName.setText(controller.getProfiles().getFullname());
        groups.setText(controller.getProfiles().getGroups());

        userAvatar.setOnClickListener(this);
    }

    boolean isOpen() {
        return mDrawer.isDrawerOpen();
    }

    void destroy() {
        mDrawer.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
        isDestroyed = true;
    }

    public void setSelectedNavigationDrawerItem(int position) {
        mDrawer.setSelectionAtPosition(position);
    }

    public void restore() {
        mDrawer.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        BusProvider.getBus().post(new Events.DisableBackArrow());
        mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        isDestroyed = false;
    }

    void closeDrawer() {
        mDrawer.closeDrawer();
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        closeDrawer();
        Fragment mFragment;
        switch ((int) drawerItem.getIdentifier()) {
            case 1:
                mFragment = new NewsFragment();
                BusProvider.getBus().post(new Events.OpenScreen(mFragment, NewsFragment.TAG, false));
                break;
            case 2:
                mFragment = new ScheduleFragment();
                BusProvider.getBus().post(new Events.OpenScreen(mFragment, ScheduleFragment.TAG, false));
                break;
            case 4:
                mFragment = new FriendsFragment();
                BusProvider.getBus().post(new Events.OpenScreen(mFragment, FriendsFragment.TAG, false));
                break;
            case 7:
                mFragment = new LoginFragment().newInstance(true);
                BusProvider.getBus().post(new Events.ClearAllModels());
                BusProvider.getBus().post(new Events.OpenScreen(mFragment, LoginFragment.TAG, false));
                break;
        }
        return true;
    }

    @Override
    public boolean onNavigationClickListener(View clickedView) {
        BusProvider.getBus().post(new Events.NavigateBack());
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_avatar:
                closeDrawer();
                StatsFragment statsFragment = new StatsFragment();
                BusProvider.getBus().post(new Events.OpenScreen(statsFragment, StatsFragment.TAG, false));
                break;
        }
    }
}
