package ru.vasidmi.technoparkclient;

import android.support.v4.app.Fragment;

public class Events {
    public static class ChangeToolbarTitle {

        private int titleId;
        private String titleString;

        public ChangeToolbarTitle(int title) {
            this.titleId = title;
        }

        public ChangeToolbarTitle(String title) {
            this.titleString = title;
        }

        public Object getTitle() {
            if (titleString == null) {
                return titleId;
            }
            return titleString;
        }
    }

    public static class CreateNavigationDrawer {

        private int selectedItem;

        public CreateNavigationDrawer(int selectedItem) {
            this.selectedItem = selectedItem;
        }

        public int getSelectedItem() {
            return selectedItem;
        }
    }


    public static class DestroyNavigationDrawer {
        public DestroyNavigationDrawer() {
        }
    }

    public static class DisableBackArrow {
        public DisableBackArrow() {
        }
    }

    public static class EnableBackArrow {
        public EnableBackArrow() {
        }
    }

    static class ClearAllModels {
        ClearAllModels() {
        }
    }

    static class NavigateBack {
        NavigateBack() {
        }
    }

    public static class LoadMainData {

        String authHeader;

        public LoadMainData(String authHeader) {
            this.authHeader = authHeader;
        }

        public String getAuthHeader() {
            return authHeader;
        }
    }

    public static class OpenScreen {

        private Fragment fragment;
        private String tag;
        private boolean backStack;

        public OpenScreen(Fragment fragment, String tag, boolean backStack) {
            this.fragment = fragment;
            this.tag = tag;
            this.backStack = backStack;
        }

        public Fragment getFragment() {
            return fragment;
        }

        public String getTag() {
            return tag;
        }

        public boolean isBackStack() {
            return backStack;
        }
    }

    public static class SetupToolbar {
        public SetupToolbar() {
        }
    }
}
