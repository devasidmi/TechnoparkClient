package ru.vasidmi.technoparkclient;

//import android.content.Context;
//import android.content.SharedPreferences;
import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class Auth {


    public String getAuthHeader(String auth) {
        return "Basic " + encodeCredentials(auth);
    }

//    public String getEncodeCredentials(String auth) {
//        return encodeCredentials(auth);
//    }
//
//    public static String decodeCredentials(String auth) {
//        byte[] decodedBytesAuth = Base64.decode(auth, Base64.NO_WRAP);
//        String decodedAuth = "";
//        try {
//            decodedAuth = new String(decodedBytesAuth, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        return decodedAuth;
//    }

     private String encodeCredentials(String auth) {
        String authHeader = "";
        try {
            byte[] authHeaderBytes = Base64.encode(auth.getBytes("UTF-8"), Base64.NO_WRAP);
            authHeader = new String(authHeaderBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return authHeader;
    }
}
