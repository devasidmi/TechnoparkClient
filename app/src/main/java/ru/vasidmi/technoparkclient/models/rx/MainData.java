package ru.vasidmi.technoparkclient.models.rx;

import java.util.List;

import ru.vasidmi.technoparkclient.models.NewsModel;
import ru.vasidmi.technoparkclient.models.ProfileModel;
import ru.vasidmi.technoparkclient.models.friends.FriendsModel;
import ru.vasidmi.technoparkclient.models.progress.ProgressModel;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class MainData {


    private ProfileModel profile;
    private FriendsModel friends;
    private List<NewsModel> news;
    private List<ProgressModel> progress;

    public MainData(ProfileModel profile, FriendsModel friends, List<NewsModel> news, List<ProgressModel> progressModels) {
        this.profile = profile;
        this.friends = friends;
        this.news = news;
        this.progress = progressModels;
    }

    public ProfileModel getProfile() {
        return profile;
    }

    public List<NewsModel> getNews() {
        return news;
    }


    public FriendsModel getFriends() {
        return friends;
    }

    public List<ProgressModel> getProgress() {
        return progress;
    }

    public void setProgress(List<ProgressModel> progress) {
        this.progress = progress;
    }
}
