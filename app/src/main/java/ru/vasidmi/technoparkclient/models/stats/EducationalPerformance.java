package ru.vasidmi.technoparkclient.models.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class EducationalPerformance {

    @SerializedName("in_kit")
    @Expose
    private String inKit;
    @SerializedName("in_semester")
    @Expose
    private String inSemester;

    public String getInKit() {
        return inKit;
    }

    public void setInKit(String inKit) {
        this.inKit = inKit;
    }

    public String getInSemester() {
        return inSemester;
    }

    public void setInSemester(String inSemester) {
        this.inSemester = inSemester;
    }

}
