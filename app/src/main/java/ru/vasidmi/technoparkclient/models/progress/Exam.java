package ru.vasidmi.technoparkclient.models.progress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class Exam {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("max")
    @Expose
    private String max;
    @SerializedName("user_got")
    @Expose
    private String userGot;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getUserGot() {
        return userGot;
    }

    public void setUserGot(String userGot) {
        this.userGot = userGot;
    }

}
