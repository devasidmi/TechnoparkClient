package ru.vasidmi.technoparkclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ru.vasidmi.technoparkclient.models.stats.PortalActivity;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class ProfileModel implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("user_about")
    @Expose
    private String userAbout;
    @SerializedName("main_group")
    @Expose
    private String mainGroup;
    @SerializedName("groups")
    @Expose
    private String groups;
    @SerializedName("online")
    @Expose
    private Boolean online;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("bdate")
    @Expose
    private String bdate;
    @SerializedName("registered")
    @Expose
    private String registered;
    @SerializedName("last_seen")
    @Expose
    private String lastSeen;
    @SerializedName("portal_activity")
    @Expose
    private PortalActivity portalActivity;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUserAbout() {
        return userAbout;
    }

    public void setUserAbout(String userAbout) {
        this.userAbout = userAbout;
    }

    public String getMainGroup() {
        return mainGroup;
    }

    public void setMainGroup(String mainGroup) {
        this.mainGroup = mainGroup;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public PortalActivity getPortalActivity() {
        return portalActivity;
    }

    public void setPortalActivity(PortalActivity portalActivity) {
        this.portalActivity = portalActivity;
    }

}
