package ru.vasidmi.technoparkclient.models.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class StatsModel implements Serializable {

    @SerializedName("portal_activity")
    @Expose
    private PortalActivity portalActivity;
    @SerializedName("educational_performance")
    @Expose
    private EducationalPerformance educationalPerformance;
    @SerializedName("attendance")
    @Expose
    private Attendance attendance;
    @SerializedName("achievements")
    @Expose
    private List<Achievement> achievements = null;

    public PortalActivity getPortalActivity() {
        return portalActivity;
    }

    public void setPortalActivity(PortalActivity portalActivity) {
        this.portalActivity = portalActivity;
    }

    public EducationalPerformance getEducationalPerformance() {
        return educationalPerformance;
    }

    public void setEducationalPerformance(EducationalPerformance educationalPerformance) {
        this.educationalPerformance = educationalPerformance;
    }

    public Attendance getAttendance() {
        return attendance;
    }

    public void setAttendance(Attendance attendance) {
        this.attendance = attendance;
    }

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Achievement> achievements) {
        this.achievements = achievements;
    }
}