package ru.vasidmi.technoparkclient.models.friends;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class FriendsModel implements Serializable {

    @SerializedName("count")
    @Expose
    private String count;
    @SerializedName("friends")
    @Expose
    private List<Friend> friends = null;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

}
