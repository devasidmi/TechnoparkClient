package ru.vasidmi.technoparkclient.models.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class Attendance {

    @SerializedName("in_semester")
    @Expose
    private String inSemester;
    @SerializedName("percent")
    @Expose
    private String percent;

    public String getInSemester() {
        return inSemester;
    }

    public void setInSemester(String inSemester) {
        this.inSemester = inSemester;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

}
