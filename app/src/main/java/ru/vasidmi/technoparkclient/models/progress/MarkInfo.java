package ru.vasidmi.technoparkclient.models.progress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class MarkInfo {

    @SerializedName("badFrom")
    @Expose
    private String badFrom;
    @SerializedName("satisfactoryFrom")
    @Expose
    private String satisfactoryFrom;
    @SerializedName("goodFrom")
    @Expose
    private String goodFrom;
    @SerializedName("excellentFrom")
    @Expose
    private String excellentFrom;

    public String getBadFrom() {
        return badFrom;
    }

    public void setBadFrom(String badFrom) {
        this.badFrom = badFrom;
    }

    public String getSatisfactoryFrom() {
        return satisfactoryFrom;
    }

    public void setSatisfactoryFrom(String satisfactoryFrom) {
        this.satisfactoryFrom = satisfactoryFrom;
    }

    public String getGoodFrom() {
        return goodFrom;
    }

    public void setGoodFrom(String goodFrom) {
        this.goodFrom = goodFrom;
    }

    public String getExcellentFrom() {
        return excellentFrom;
    }

    public void setExcellentFrom(String excellentFrom) {
        this.excellentFrom = excellentFrom;
    }

}
