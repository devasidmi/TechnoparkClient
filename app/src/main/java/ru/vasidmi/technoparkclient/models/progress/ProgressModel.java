package ru.vasidmi.technoparkclient.models.progress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vasidmi on 21/03/2018.
 */

public class ProgressModel {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("mark")
    @Expose
    private String mark;
    @SerializedName("markInfo")
    @Expose
    private MarkInfo markInfo;
    @SerializedName("exams")
    @Expose
    private List<Exam> exams = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public MarkInfo getMarkInfo() {
        return markInfo;
    }

    public void setMarkInfo(MarkInfo markInfo) {
        this.markInfo = markInfo;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

}