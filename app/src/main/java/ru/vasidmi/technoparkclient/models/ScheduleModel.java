package ru.vasidmi.technoparkclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class ScheduleModel implements Serializable {

    @SerializedName("disciplineLink")
    @Expose
    private String disciplineLink;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("tutorLink")
    @Expose
    private String tutorLink;
    @SerializedName("groups")
    @Expose
    private String groups;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("lesson")
    @Expose
    private String lesson;
    @SerializedName("tutorName")
    @Expose
    private String tutorName;
    @SerializedName("discipline")
    @Expose
    private String discipline;
    @SerializedName("tutorImage")
    @Expose
    private String tutorImage;
    @SerializedName("future")
    @Expose
    private Boolean future;
    @SerializedName("place")
    @Expose
    private String place;
    @SerializedName("endTime")
    @Expose
    private String endTime;

    public String getDisciplineLink() {
        return disciplineLink;
    }

    public void setDisciplineLink(String disciplineLink) {
        this.disciplineLink = disciplineLink;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTutorLink() {
        return tutorLink;
    }

    public void setTutorLink(String tutorLink) {
        this.tutorLink = tutorLink;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getTutorName() {
        return tutorName;
    }

    public void setTutorName(String tutorName) {
        this.tutorName = tutorName;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public String getTutorImage() {
        return tutorImage;
    }

    public void setTutorImage(String tutorImage) {
        this.tutorImage = tutorImage;
    }

    public Boolean getFuture() {
        return future;
    }

    public void setFuture(Boolean future) {
        this.future = future;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
