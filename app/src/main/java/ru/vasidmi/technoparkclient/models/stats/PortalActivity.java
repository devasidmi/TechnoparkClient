package ru.vasidmi.technoparkclient.models.stats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vasidmi on 19/03/2018.
 */

public class PortalActivity {

    @SerializedName("power")
    @Expose
    private String power;
    @SerializedName("rating")
    @Expose
    private String rating;

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

}
