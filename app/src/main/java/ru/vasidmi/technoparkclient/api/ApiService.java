package ru.vasidmi.technoparkclient.api;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import ru.vasidmi.technoparkclient.models.NewsModel;
import ru.vasidmi.technoparkclient.models.ProfileModel;
import ru.vasidmi.technoparkclient.models.ScheduleModel;
import ru.vasidmi.technoparkclient.models.friends.FriendsModel;
import ru.vasidmi.technoparkclient.models.progress.ProgressModel;
import ru.vasidmi.technoparkclient.models.stats.StatsModel;

/**
 * Created by vasidmi on 19/03/2018.
 */

public interface ApiService {

    @GET("profile")
    Observable<ProfileModel> getOwnProfile(@Header("Authorization") String auth);

    @GET("profile/{userId}")
    Call<ProfileModel> getUserProfile(@Header("Authorization") String auth,
                                      @Path("userId") String userId);

    @GET("news")
    Observable<List<NewsModel>> getNews(@Header("Authorization") String auth);

    @GET("schedule")
    Call<List<ScheduleModel>> getSchedule(@Header("Authorization") String auth);

    @GET("friends")
    Observable<FriendsModel> getOwnFriends(@Header("Authorization") String auth);

//    @GET("friends/{userId}")
//    Observable<FriendsModel> getUserFriends(@Header("Authorization") String auth,
//                                            @Path("userId") String userId);

    @GET("stats")
    Call<StatsModel> getStats(@Header("Authorization") String auth);

    @GET("progress")
    Observable<List<ProgressModel>> getProgress(@Header("Authorization") String auth);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://185.246.67.112:5000/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();
}
