package ru.vasidmi.technoparkclient;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.squareup.otto.Subscribe;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function4;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import ru.vasidmi.technoparkclient.fragments.LoginFragment;
import ru.vasidmi.technoparkclient.fragments.NewsFragment;
import ru.vasidmi.technoparkclient.models.NewsModel;
import ru.vasidmi.technoparkclient.models.ProfileModel;
import ru.vasidmi.technoparkclient.models.ScheduleModel;
import ru.vasidmi.technoparkclient.models.friends.FriendsModel;
import ru.vasidmi.technoparkclient.models.progress.ProgressModel;
import ru.vasidmi.technoparkclient.models.rx.MainData;


public class MainActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;
    private LoginFragment loginFragment;
    private Toolbar mToolbar;
    private ProfileModel profiles;
    private FriendsModel friends;
    private List<NewsModel> news;
    private List<ScheduleModel> schedule;
    private NavigationDrawer mNavigationDrawer;
    private AppCompatActivity activity;
    private CompositeDisposable compositeDisposable;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        BusProvider.getBus().register(this);
        loginFragment = new LoginFragment().newInstance(false);
        mToolbar = findViewById(R.id.toolbar);
        activity = this;
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_container, loginFragment,
                LoginFragment.TAG);
        mFragmentTransaction.commit();
    }

    public NavigationDrawer getNavigationDrawer() {
        return mNavigationDrawer;
    }

    @Subscribe
    public void onChangeToolbarTitle(Events.ChangeToolbarTitle event) {
        Object title = event.getTitle();
        if (title instanceof Integer) {
            mToolbar.setTitle((Integer) title);
        } else {
            mToolbar.setTitle((CharSequence) title);
        }
    }

    @Subscribe
    public void onClearAllModels(Events.ClearAllModels event) {
        profiles = null;
        friends = null;
        schedule = null;
    }

    @Subscribe
    public void onSetupToolbar(Events.SetupToolbar event) {
        activity.setSupportActionBar(mToolbar);
    }

    @Subscribe
    public void onDestroyNavigationDrawer(Events.DestroyNavigationDrawer event) {
        mNavigationDrawer.destroy();
    }

    @Subscribe
    public void onCreateNavigationDrawer(Events.CreateNavigationDrawer event) {
        mNavigationDrawer = new NavigationDrawer(this);
        int selectedItem = event.getSelectedItem();
        mNavigationDrawer.createDrawer(activity, R.id.rootView, selectedItem);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public ProfileModel getProfiles() {
        return profiles;
    }

    public List<NewsModel> getNews() {
        return news;
    }

    public FriendsModel getFriends() {
        return friends;
    }

    public List<ScheduleModel> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<ScheduleModel> schedule) {
        this.schedule = schedule;
    }

    @Subscribe
    public void onOpenScreen(Events.OpenScreen event) {
        String tag = event.getTag();
        Fragment fragment = event.getFragment();
        boolean backStack = event.isBackStack();

        Fragment currentFragment = mFragmentManager.findFragmentById(R.id.fragment_container);
        if (!tag.equals(currentFragment.getTag())) {
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.fragment_container, fragment, tag);
            if (backStack)
                mFragmentTransaction.addToBackStack(tag);
            mFragmentTransaction.commit();
        }
    }

    @Subscribe
    public void onEnableBackArrow(Events.EnableBackArrow event) {
        ActionBar supportActionBar = activity.getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Subscribe
    public void onDisableBackArrow(Events.DisableBackArrow event) {
        ActionBar supportActionBar = activity.getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Subscribe
    public void onNavigateBack(Events.NavigateBack event) {
        mFragmentManager.popBackStack();
    }

    @Subscribe
    public void onLoadMainData(Events.LoadMainData event) {
        compositeDisposable = new CompositeDisposable();
        MainDataObservables mainDataObservables;
        final String authHeader = event.getAuthHeader();
        mainDataObservables = new MainDataObservables(authHeader);
        Observable<MainData> comb = Observable.zip(
                mainDataObservables.profileObser,
                mainDataObservables.newsObser,
                mainDataObservables.friendsObser,
                mainDataObservables.progressObser,
                new Function4<ProfileModel, List<NewsModel>, FriendsModel, List<ProgressModel>, MainData>() {
                    @Override
                    public MainData apply(ProfileModel profileModel, List<NewsModel> newsModels, FriendsModel friendsModel, List<ProgressModel> progressModels) {
                        return new MainData(profileModel, friendsModel, newsModels, progressModels);
                    }
                }
        );
        compositeDisposable.add(comb.takeUntil(Observable.timer(150000L, TimeUnit.MILLISECONDS))
                .subscribe(new Consumer<MainData>() {
                    @Override
                    public void accept(MainData mainData) {
                        saveAuthHeader(authHeader);
                        profiles = mainData.getProfile();
                        friends = mainData.getFriends();
                        news = mainData.getNews();
                        Fragment newsFragment = new NewsFragment();
                        BusProvider.getBus().post(new Events.OpenScreen(newsFragment, NewsFragment.TAG, false));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        String errMsg = mContext.getResources().getString(R.string.connection_error);
                        loginFragment.enableLoginBtn();
                        int code;
                        try {
                            code = ((HttpException) throwable).code();
                            ResponseBody errorBody = ((HttpException) throwable).response().errorBody();
                            if (errorBody != null)
                                errMsg = errorBody.string();
                            switch (code) {
                                case 401:
                                    new LoginFragment().showLoginError(errMsg);
                                    break;
                                default:
                                    new LoginFragment().showLoginError(errMsg);
                                    Log.e("YAY!", "Error");
                            }
                        } catch (Exception e) {
                            Log.e("YAY!", "connection error");
                        }
                    }
                }));
    }

    public void saveAuthHeader(String authHeader) {
        SharedPreferences mSharedPreferences = getSharedPreferences("session_prefs", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putString("auth_header", authHeader);
        mEditor.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getBus().unregister(this);
        Log.e("YAY!", "onPause MainActivity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
        Log.e("YAY!", "onDestroy MainActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            BusProvider.getBus().register(this);
        } catch (Exception e) {
            Log.e("YAY!", "Bus already registered!");
        }
        Log.e("YAY!", "onResume MainActivity");
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawer != null) {
            if (mNavigationDrawer.isOpen()) {
                mNavigationDrawer.closeDrawer();
                return;
            }
        }
        super.onBackPressed();
    }
}
